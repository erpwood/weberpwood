import Vue from 'vue'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import 'babel-polyfill'
import 'url-search-params-polyfill'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import '@/styles/index.scss' // global css
import App from './App'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import '@/icons' // icon
import '@/permission' // permission control
import '../node_modules/bulma/bulma.sass'
import Multiselect from 'vue-multiselect'
import '@mdi/font/css/materialdesignicons.css'
import '../node_modules/vue-multiselect/dist/vue-multiselect.min.css'
import moment from 'moment-timezone'
import tinymce from 'vue-tinymce-editor'
import vueXlsxTable from 'vue-xlsx-table'
Vue.component('tinymce', tinymce)
// import { VueEditor } from 'vue2-editor'
// moment.tz.setDefault('Asia/Jakarta')
Vue.prototype.$moment = moment
Vue.use(ElementUI, { locale })
Vue.use(Buefy)
Vue.config.productionTip = false
Vue.component('multiselect', Multiselect)
Vue.use(vueXlsxTable, { rABS: false })
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
