import request from '@/utils/request'

export function login(username, password) {
  const params = new URLSearchParams()
  params.append('email', username)
  params.append('password', password)
  // params.append('role', 'admin')
  return request({
    url: 'user/login',
    method: 'post',
    data: params,
    headers: {
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
