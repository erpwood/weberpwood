import { login } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    admin_id: '',
    client_id: '',
    isLogedIn: false
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_ADMINID: (state, admin_id) => {
      state.admin_id = admin_id
    },
    SET_CLIENTID: (state, client_id) => {
      state.client_id = client_id
    },
    SET_ISLOGEDIN: (state, isLogedIn) => {
      state.isLogedIn = isLogedIn
    }
  },

  actions: {
    // log in
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          setToken('admin')
          commit('SET_TOKEN', 'admin')
          commit('SET_NAME', response.data[0].username)
          commit('SET_ROLES', response.data[0].role)
          commit('SET_ADMINID', response.data[0]._id)
          commit('SET_CLIENTID', response.data[0].client_id)
          commit('SET_ISLOGEDIN', true)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Get user information
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        commit('SET_AVATAR', 'avatar')
        resolve()
        // getInfo(state.token).then(response => {
        // const data = response.data
        // if (data.roles && data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
        // commit('SET_ROLES', data.roles)
        // } else {
        // reject('getInfo: roles must be a non-null array !')
        // }
        // commit('SET_NAME', data.name)
        // commit('SET_AVATAR', data.avatar)
        // resolve(response)
        // }).catch(error => {
        // reject(error)
        // })
      })
    },

    // LogOut
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        commit('SET_NAME', '')
        commit('SET_ADMINID', '')
        commit('SET_CLIENTID', '')
        commit('SET_ISLOGEDIN', false)
        removeToken()
        resolve()
        // logout('admin').then(() => {
        // commit('SET_TOKEN', '')
        // commit('SET_ROLES', [])
        // removeToken()
        // resolve()
        // }).catch(error => {
        // reject(error)
        // })
      })
    },

    //  Front end
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
