import axios from 'axios'
import { Message } from 'element-ui'
import store from '../store'
import { getToken } from '@/utils/auth'

// Create an axios instance
const service = axios.create({
  baseURL: process.env.BASE_API // Request timeout
})

// Request interceptor
service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      config.headers['X-Token'] = getToken() // Let each request carry a custom token, please modify it according to the actual situation
    }
    return config
  },
  error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// Response interceptor
service.interceptors.response.use(
  response => {
    /**
     * Code is non-20000 is a mistake can be combined with their own business to modify
     */
    const res = response.data
    if (res.status !== undefined && res.status.trim().toUpperCase() !== 'SUCCESS') {
      Message({
        message: res.status,
        type: 'error',
        duration: 5 * 1000
      })
      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      return Promise.reject('error')
    } else if (res.status === undefined && res.email !== undefined) {
      Message({
        message: res.email[0],
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(res.email[0])
    } else {
      return response.data
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
