'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  //BASE_API: '"https://api.scandev.subtlelabs.com/api"',
  BASE_API: '"http://erpwood.com:8000/api"',  /// localhost
})
